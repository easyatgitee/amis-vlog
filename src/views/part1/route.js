const Layout = () => import('@/layout/index.vue')

export default {
  name: 'MultipleMenu',
  path: '/multi-menu',
  component: Layout,
  meta: {
    title: '文章',
    icon: 'ic:baseline-menu',
    role: ['admin'],
    requireAuth: true,
    order: 4,
  },
  children: [
    {
      name: 'a-1',
      path: 'a-1',
      component: () => import('./page1/index.vue'),
      meta: {
        title: '写文章', 
        role: ['admin'],
        requireAuth: true,
      },
    },
    {
      name: 'a-2',
      path: 'a-2',
      component: () => import('./page2/index.vue'),
      meta: {
        title: '文章', 
        role: ['admin'],
        requireAuth: true,
      },
    },
    {
      name: 'a-3',
      path: 'a-3',
      component: () => import('./page3/index.vue'),
      meta: {
        title: '草稿',
        role: ['admin'],
        requireAuth: true,
      },
    },
    {
      name: 'a-4',
      path: 'a-4',
      component: () => import('./page4/index.vue'),
      meta: {
        title: '分类',
        role: ['admin'],
        requireAuth: true,
      },
    },
    {
      name: 'a-5',
      path: 'a-5',
      component: () => import('./page5/index.vue'),
      meta: {
        title: '标签',
        role: ['admin'],
        requireAuth: true,
      },
    },
  ],
}
